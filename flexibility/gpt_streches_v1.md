Sunday

Sit and Reach
Seated Calf Stretch
Pigeon Pose
Couch pose
Frog Pose
Prone Quad Stretch

Extra - Half splits


Monday

Cow Face Arms
Eagle Arms
Supine Shoulder Ext
Lying Spinal Twist (II)
Seated Neck Release
Child’s Pose

Extra - Wall scorpion


Tuesday

Bent over reach (strap)
Downward dog (block)
Pigeon Pose Quad Stretch (strap)
Seated Figure Four Stretch
Lizard Pose
Happy baby


Wednesday

Happy Baby Pose
Low Lunge
Bound Angle Pose
Lying Supine Twist (I)
Camel Pose
Seated Wide Angle Pose
Child's pose


Thursday

Head to knees
Extend wide squat
Butterfly
Seated Calf Stretch
Pigeon (double)
Wide legged split

Extra - Half splits


Friday

Chest door
Shoulder door
Seated Spinal Twist
Overhead Pigeon Quad Stretch (strap)
Cow Face Arms
Supine Shoulder Ext


Saturday

Trap Ear Pull Stretch
Cross-body shoulder
Fish Pose
Superman
Seated Heart Opener
Seated Forward Bend

Extra - Wall Angels