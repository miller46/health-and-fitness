Sunday: Lower Body Focus
Stretch
Sit and Reach
Seated Calf Stretch
Prone Quad Stretch
Pigeon Pose
Warrior I Pose
Frog Pose


Monday: Lower Body Focus
Stretch
Lizard Pose
Downward Dog
Half Splits Pose
Figure Four Stretch
Supine Hand to Big Toe Pose
Butterfly Pose

Tuesday: Upper Body & Spine Focus
Stretch
Cow Face Arms
Eagle Arms
Supine Shoulder Extension
Lying Spinal Twist (II)
Seated Neck Release
Child’s Pose

Wednesday: Hips & Core
Stretch
Happy Baby Pose
Low Lunge
Bound Angle Pose
Lying Supine Twist (I)
Camel Pose
Seated Wide Angle Pose

Thursday: Full Body
Stretch
Triangle Pose
Seated Spinal Twist
Camel Pose
Downward Dog
Wide-legged Forward Bend
Thread the Needle

Friday: Deep Stretch
Stretch
Puppy Pose
Reclining Bound Angle Pose
King Pigeon Pose
Fish Pose
Prone Chest Stretch
Seated Forward Bend


Saturday: Rest